﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace testApp.Models {
    public class TestAppDBContext : DbContext {

        public TestAppDBContext(DbContextOptions options) : base(options) {

        }


        public DbSet<Supervisor> Supervisors { get; set; }
        public DbSet<Student> Students { get; set; }
    }
}
