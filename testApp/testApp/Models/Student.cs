﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace testApp.Models {
    public class Student {

        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Supervisor Supervisor { get; set; }

        public int SupervisorId { get; set; }


    }
}
